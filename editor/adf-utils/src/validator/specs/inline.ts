export default [
  'hardBreak',
  'mention',
  'emoji',
  'inlineExtension_with_marks',
  'inlineExtension_with_experimental_marks',
  'date',
  'placeholder',
  'inlineCard',
  'status',
  'mediaInline',
  'formatted_text_inline',
  'code_inline',
];
