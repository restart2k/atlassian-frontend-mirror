export default [
  'extension',
  { props: { marks: { type: 'array', items: ['fragment'], optional: true } } },
];
