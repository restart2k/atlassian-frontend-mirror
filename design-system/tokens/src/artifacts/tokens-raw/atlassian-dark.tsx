
// THIS IS AN AUTO-GENERATED FILE DO NOT MODIFY DIRECTLY
// Re-generate by running `yarn build tokens`.

const tokens = [
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for blue backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#0C66E4",
    "filePath": "src/tokens/atlassian-dark/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for blue backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
      },
      "value": "B700"
    },
    "name": "color.accent.boldBlue",
    "path": [
      "color",
      "accent",
      "boldBlue"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for green backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#1F845A",
    "filePath": "src/tokens/atlassian-dark/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for green backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
      },
      "value": "G700"
    },
    "name": "color.accent.boldGreen",
    "path": [
      "color",
      "accent",
      "boldGreen"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for orange backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#B65C02",
    "filePath": "src/tokens/atlassian-dark/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for orange backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
      },
      "value": "O700"
    },
    "name": "color.accent.boldOrange",
    "path": [
      "color",
      "accent",
      "boldOrange"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for purple backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#6E5DC6",
    "filePath": "src/tokens/atlassian-dark/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for purple backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
      },
      "value": "P700"
    },
    "name": "color.accent.boldPurple",
    "path": [
      "color",
      "accent",
      "boldPurple"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for red backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#CA3521",
    "filePath": "src/tokens/atlassian-dark/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for red backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
      },
      "value": "R700"
    },
    "name": "color.accent.boldRed",
    "path": [
      "color",
      "accent",
      "boldRed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for teal backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#1D7F8C",
    "filePath": "src/tokens/atlassian-dark/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for teal backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
      },
      "value": "T700"
    },
    "name": "color.accent.boldTeal",
    "path": [
      "color",
      "accent",
      "boldTeal"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for blue subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#09326C",
    "filePath": "src/tokens/atlassian-dark/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for blue subdued backgrounds when there is no meaning tied to the color, such as colored tags."
      },
      "value": "B900"
    },
    "name": "color.accent.subtleBlue",
    "path": [
      "color",
      "accent",
      "subtleBlue"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for green subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#164B35",
    "filePath": "src/tokens/atlassian-dark/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for green subdued backgrounds when there is no meaning tied to the color, such as colored tags."
      },
      "value": "G900"
    },
    "name": "color.accent.subtleGreen",
    "path": [
      "color",
      "accent",
      "subtleGreen"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for magenta subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#50253F",
    "filePath": "src/tokens/atlassian-dark/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for magenta subdued backgrounds when there is no meaning tied to the color, such as colored tags."
      },
      "value": "M900"
    },
    "name": "color.accent.subtleMagenta",
    "path": [
      "color",
      "accent",
      "subtleMagenta"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for orange subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#5F3811",
    "filePath": "src/tokens/atlassian-dark/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for orange subdued backgrounds when there is no meaning tied to the color, such as colored tags."
      },
      "value": "O900"
    },
    "name": "color.accent.subtleOrange",
    "path": [
      "color",
      "accent",
      "subtleOrange"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for purple subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#352C63",
    "filePath": "src/tokens/atlassian-dark/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for purple subdued backgrounds when there is no meaning tied to the color, such as colored tags."
      },
      "value": "P900"
    },
    "name": "color.accent.subtlePurple",
    "path": [
      "color",
      "accent",
      "subtlePurple"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for red subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#601E16",
    "filePath": "src/tokens/atlassian-dark/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for red subdued backgrounds when there is no meaning tied to the color, such as colored tags."
      },
      "value": "R900"
    },
    "name": "color.accent.subtleRed",
    "path": [
      "color",
      "accent",
      "subtleRed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for teal subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#1D474C",
    "filePath": "src/tokens/atlassian-dark/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for teal subdued backgrounds when there is no meaning tied to the color, such as colored tags."
      },
      "value": "T900"
    },
    "name": "color.accent.subtleTeal",
    "path": [
      "color",
      "accent",
      "subtleTeal"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as a secondary background for the UI"
    },
    "value": "#03040442",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use as a secondary background for the UI"
      },
      "value": "DN-100A"
    },
    "name": "color.background.sunken",
    "path": [
      "color",
      "background",
      "sunken"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as the primary background for the UI"
    },
    "value": "#161A1D",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use as the primary background for the UI"
      },
      "value": "DN0"
    },
    "name": "color.background.default",
    "path": [
      "color",
      "background",
      "default"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for the background of raised cards, such as Jira cards on a Kanban board.\nCombine with shadow.card."
    },
    "value": "#1D2125",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for the background of raised cards, such as Jira cards on a Kanban board.\nCombine with shadow.card."
      },
      "value": "DN100"
    },
    "name": "color.background.card",
    "path": [
      "color",
      "background",
      "card"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse for the background of overlay elements, such as modals, dropdown menus, flags, and inline dialogs (i.e. elements that sit on top of the UI).\n\nAlso use for the background of raised cards in a dragged state.\n\nCombine with shadow.overlay."
    },
    "value": "#22272B",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "\nUse for the background of overlay elements, such as modals, dropdown menus, flags, and inline dialogs (i.e. elements that sit on top of the UI).\n\nAlso use for the background of raised cards in a dragged state.\n\nCombine with shadow.overlay."
      },
      "value": "DN200"
    },
    "name": "color.background.overlay",
    "path": [
      "color",
      "background",
      "overlay"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for backgrounds of elements in a selected state"
    },
    "value": "#A1BDD914",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for backgrounds of elements in a selected state"
      },
      "value": "DN200A"
    },
    "name": "color.background.selected.resting",
    "path": [
      "color",
      "background",
      "selected",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.selected"
    },
    "value": "#A6C5E229",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state of background.selected"
      },
      "value": "DN300A"
    },
    "name": "color.background.selected.hover",
    "path": [
      "color",
      "background",
      "selected",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.selected"
    },
    "value": "#BFDBF847",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state of background.selected"
      },
      "value": "DN400A"
    },
    "name": "color.background.selected.pressed",
    "path": [
      "color",
      "background",
      "selected",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for the screen overlay that appears with modal dialogs"
    },
    "value": "#03040442",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for the screen overlay that appears with modal dialogs"
      },
      "value": "DN-100A"
    },
    "name": "color.background.blanket",
    "path": [
      "color",
      "background",
      "blanket"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for backgrounds of elements in a disabled state"
    },
    "value": "#A1BDD914",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for backgrounds of elements in a disabled state"
      },
      "value": "DN200A"
    },
    "name": "color.background.disabled",
    "path": [
      "color",
      "background",
      "disabled"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like primary buttons and bold in progress lozenges."
    },
    "value": "#579DFF",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "A vibrant background for small UI elements like primary buttons and bold in progress lozenges."
      },
      "value": "B400"
    },
    "name": "color.background.boldBrand.resting",
    "path": [
      "color",
      "background",
      "boldBrand",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldBrand"
    },
    "value": "#85B8FF",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state of background.boldBrand"
      },
      "value": "B300"
    },
    "name": "color.background.boldBrand.hover",
    "path": [
      "color",
      "background",
      "boldBrand",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldBrand"
    },
    "value": "#CCE0FF",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state of background.boldBrand"
      },
      "value": "B200"
    },
    "name": "color.background.boldBrand.pressed",
    "path": [
      "color",
      "background",
      "boldBrand",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like information section messages and in progress lozenges."
    },
    "value": "#082145",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for subdued backgrounds of UI elements like information section messages and in progress lozenges."
      },
      "value": "B1000"
    },
    "name": "color.background.subtleBrand.resting",
    "path": [
      "color",
      "background",
      "subtleBrand",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleBrand"
    },
    "value": "#09326C",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state for background.subtleBrand"
      },
      "value": "B900"
    },
    "name": "color.background.subtleBrand.hover",
    "path": [
      "color",
      "background",
      "subtleBrand",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleBrand"
    },
    "value": "#0055CC",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state for background.subtleBrand"
      },
      "value": "B800"
    },
    "name": "color.background.subtleBrand.pressed",
    "path": [
      "color",
      "background",
      "subtleBrand",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like danger buttons and bold removed lozenges."
    },
    "value": "#F87462",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "A vibrant background for small UI elements like danger buttons and bold removed lozenges."
      },
      "value": "R400"
    },
    "name": "color.background.boldDanger.resting",
    "path": [
      "color",
      "background",
      "boldDanger",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldDanger"
    },
    "value": "#FF9C8F",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state of background.boldDanger"
      },
      "value": "R300"
    },
    "name": "color.background.boldDanger.hover",
    "path": [
      "color",
      "background",
      "boldDanger",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldDanger"
    },
    "value": "#FFD2CC",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state of background.boldDanger"
      },
      "value": "R200"
    },
    "name": "color.background.boldDanger.pressed",
    "path": [
      "color",
      "background",
      "boldDanger",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like error section messages and removed lozenges."
    },
    "value": "#391813",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for subdued backgrounds of UI elements like error section messages and removed lozenges."
      },
      "value": "R1000"
    },
    "name": "color.background.subtleDanger.resting",
    "path": [
      "color",
      "background",
      "subtleDanger",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleDanger"
    },
    "value": "#601E16",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state for background.subtleDanger"
      },
      "value": "R900"
    },
    "name": "color.background.subtleDanger.hover",
    "path": [
      "color",
      "background",
      "subtleDanger",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleDanger"
    },
    "value": "#AE2A19",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state for background.subtleDanger"
      },
      "value": "R800"
    },
    "name": "color.background.subtleDanger.pressed",
    "path": [
      "color",
      "background",
      "subtleDanger",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like warning buttons and bold moved lozenges."
    },
    "value": "#E2B203",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "A vibrant background for small UI elements like warning buttons and bold moved lozenges."
      },
      "value": "Y400"
    },
    "name": "color.background.boldWarning.resting",
    "path": [
      "color",
      "background",
      "boldWarning",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldWarning"
    },
    "value": "#F5CD47",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state of background.boldWarning"
      },
      "value": "Y300"
    },
    "name": "color.background.boldWarning.hover",
    "path": [
      "color",
      "background",
      "boldWarning",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldWarning"
    },
    "value": "#F8E6A0",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state of background.boldWarning"
      },
      "value": "Y200"
    },
    "name": "color.background.boldWarning.pressed",
    "path": [
      "color",
      "background",
      "boldWarning",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like warning section messages and moved lozenges."
    },
    "value": "#3D2E00",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for subdued backgrounds of UI elements like warning section messages and moved lozenges."
      },
      "value": "Y1000"
    },
    "name": "color.background.subtleWarning.resting",
    "path": [
      "color",
      "background",
      "subtleWarning",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleWarning"
    },
    "value": "#533F04",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state for background.subtleWarning"
      },
      "value": "Y900"
    },
    "name": "color.background.subtleWarning.hover",
    "path": [
      "color",
      "background",
      "subtleWarning",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleWarning"
    },
    "value": "#7F5F01",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state for background.subtleWarning"
      },
      "value": "Y800"
    },
    "name": "color.background.subtleWarning.pressed",
    "path": [
      "color",
      "background",
      "subtleWarning",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like checked toggles and bold success lozenges."
    },
    "value": "#4BCE97",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "A vibrant background for small UI elements like checked toggles and bold success lozenges."
      },
      "value": "G400"
    },
    "name": "color.background.boldSuccess.resting",
    "path": [
      "color",
      "background",
      "boldSuccess",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldSuccess"
    },
    "value": "#7EE2B8",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state of background.boldSuccess"
      },
      "value": "G300"
    },
    "name": "color.background.boldSuccess.hover",
    "path": [
      "color",
      "background",
      "boldSuccess",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldSuccess"
    },
    "value": "#BAF3DB",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state of background.boldSuccess"
      },
      "value": "G200"
    },
    "name": "color.background.boldSuccess.pressed",
    "path": [
      "color",
      "background",
      "boldSuccess",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like success section messages and success lozenges. "
    },
    "value": "#133527",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for subdued backgrounds of UI elements like success section messages and success lozenges. "
      },
      "value": "G1000"
    },
    "name": "color.background.subtleSuccess.resting",
    "path": [
      "color",
      "background",
      "subtleSuccess",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleSuccess"
    },
    "value": "#164B35",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state for background.subtleSuccess"
      },
      "value": "G900"
    },
    "name": "color.background.subtleSuccess.hover",
    "path": [
      "color",
      "background",
      "subtleSuccess",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleSuccess"
    },
    "value": "#216E4E",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state for background.subtleSuccess"
      },
      "value": "G800"
    },
    "name": "color.background.subtleSuccess.pressed",
    "path": [
      "color",
      "background",
      "subtleSuccess",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like onboarding buttons and bold new lozenges."
    },
    "value": "#9F8FEF",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "A vibrant background for small UI elements like onboarding buttons and bold new lozenges."
      },
      "value": "P400"
    },
    "name": "color.background.boldDiscovery.resting",
    "path": [
      "color",
      "background",
      "boldDiscovery",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldDiscovery"
    },
    "value": "#B8ACF6",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state of background.boldDiscovery"
      },
      "value": "P300"
    },
    "name": "color.background.boldDiscovery.hover",
    "path": [
      "color",
      "background",
      "boldDiscovery",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldDiscovery"
    },
    "value": "#DFD8FD",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state of background.boldDiscovery"
      },
      "value": "P200"
    },
    "name": "color.background.boldDiscovery.pressed",
    "path": [
      "color",
      "background",
      "boldDiscovery",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like discovery section messages and new lozenges."
    },
    "value": "#231C3F",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for subdued backgrounds of UI elements like discovery section messages and new lozenges."
      },
      "value": "P1000"
    },
    "name": "color.background.subtleDiscovery.resting",
    "path": [
      "color",
      "background",
      "subtleDiscovery",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleDiscovery"
    },
    "value": "#352C63",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state for background.subtleDiscovery"
      },
      "value": "P900"
    },
    "name": "color.background.subtleDiscovery.hover",
    "path": [
      "color",
      "background",
      "subtleDiscovery",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleDiscovery"
    },
    "value": "#5E4DB2",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state for background.subtleDiscovery"
      },
      "value": "P800"
    },
    "name": "color.background.subtleDiscovery.pressed",
    "path": [
      "color",
      "background",
      "subtleDiscovery",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like unchecked toggles and bold default lozenges."
    },
    "value": "#9FADBC",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "A vibrant background for small UI elements like unchecked toggles and bold default lozenges."
      },
      "value": "DN800"
    },
    "name": "color.background.boldNeutral.resting",
    "path": [
      "color",
      "background",
      "boldNeutral",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldNeutral"
    },
    "value": "#B6C2CF",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state of background.boldNeutral"
      },
      "value": "DN900"
    },
    "name": "color.background.boldNeutral.hover",
    "path": [
      "color",
      "background",
      "boldNeutral",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldNeutral"
    },
    "value": "#C7D1DB",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state of background.boldNeutral"
      },
      "value": "DN1000"
    },
    "name": "color.background.boldNeutral.pressed",
    "path": [
      "color",
      "background",
      "boldNeutral",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for UIs that don’t have a default background, such as menu items or subtle buttons."
    },
    "value": "#A1BDD914",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state for UIs that don’t have a default background, such as menu items or subtle buttons."
      },
      "value": "DN200A"
    },
    "name": "color.background.transparentNeutral.hover",
    "path": [
      "color",
      "background",
      "transparentNeutral",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for UIs that don’t have a default background, such as menu items or subtle buttons."
    },
    "value": "#A6C5E229",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state for UIs that don’t have a default background, such as menu items or subtle buttons."
      },
      "value": "DN300A"
    },
    "name": "color.background.transparentNeutral.pressed",
    "path": [
      "color",
      "background",
      "transparentNeutral",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as the default background of UI elements like buttons, lozenges, and tags."
    },
    "value": "#A1BDD914",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use as the default background of UI elements like buttons, lozenges, and tags."
      },
      "value": "DN200A"
    },
    "name": "color.background.subtleNeutral.resting",
    "path": [
      "color",
      "background",
      "subtleNeutral",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleNeutral"
    },
    "value": "#A6C5E229",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state for background.subtleNeutral"
      },
      "value": "DN300A"
    },
    "name": "color.background.subtleNeutral.hover",
    "path": [
      "color",
      "background",
      "subtleNeutral",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleNeutral"
    },
    "value": "#BFDBF847",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state for background.subtleNeutral"
      },
      "value": "DN400A"
    },
    "name": "color.background.subtleNeutral.pressed",
    "path": [
      "color",
      "background",
      "subtleNeutral",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleBorderedNeutral"
    },
    "value": "#BCD6F00A",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state for background.subtleBorderedNeutral"
      },
      "value": "DN100A"
    },
    "name": "color.background.subtleBorderedNeutral.resting",
    "path": [
      "color",
      "background",
      "subtleBorderedNeutral",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleBorderedNeutral"
    },
    "value": "#A1BDD914",
    "filePath": "src/tokens/atlassian-dark/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state for background.subtleBorderedNeutral"
      },
      "value": "DN200A"
    },
    "name": "color.background.subtleBorderedNeutral.pressed",
    "path": [
      "color",
      "background",
      "subtleBorderedNeutral",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for focus rings of elements in a focus state"
    },
    "value": "#85B8FF",
    "filePath": "src/tokens/atlassian-dark/color/border.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for focus rings of elements in a focus state"
      },
      "value": "B300"
    },
    "name": "color.border.focus",
    "path": [
      "color",
      "border",
      "focus"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use to create borders around UI elements such as text fields, checkboxes, and radio buttons, or to visually group or separate UI elements, such as flat cards or side panel dividers"
    },
    "value": "#A6C5E229",
    "filePath": "src/tokens/atlassian-dark/color/border.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use to create borders around UI elements such as text fields, checkboxes, and radio buttons, or to visually group or separate UI elements, such as flat cards or side panel dividers"
      },
      "value": "DN300A"
    },
    "name": "color.border.neutral",
    "path": [
      "color",
      "border",
      "neutral"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons or borders representing brand, in-progress, or information, such as the icons in information sections messages.\n\nAlso use for blue icons or borders when there is no meaning tied to the color, such as file type icons."
    },
    "value": "#388BFF",
    "filePath": "src/tokens/atlassian-dark/color/icon-border.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "\nUse rarely for icons or borders representing brand, in-progress, or information, such as the icons in information sections messages.\n\nAlso use for blue icons or borders when there is no meaning tied to the color, such as file type icons."
      },
      "value": "B500"
    },
    "name": "color.iconBorder.brand",
    "path": [
      "color",
      "iconBorder",
      "brand"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons and borders representing critical information, such the icons in error section messages or the borders on invalid text fields.\n\nAlso use for red icons or borders when there is no meaning tied to the color, such as file type icons."
    },
    "value": "#EF5C48",
    "filePath": "src/tokens/atlassian-dark/color/icon-border.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "\nUse rarely for icons and borders representing critical information, such the icons in error section messages or the borders on invalid text fields.\n\nAlso use for red icons or borders when there is no meaning tied to the color, such as file type icons."
      },
      "value": "R500"
    },
    "name": "color.iconBorder.danger",
    "path": [
      "color",
      "iconBorder",
      "danger"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons and borders representing semi-urgent information, such as the icons in warning section messages.\n\nAlso use for yellow icons or borders when there is no meaning tied to the color, such as file type icons.\n"
    },
    "value": "#CF9F02",
    "filePath": "src/tokens/atlassian-dark/color/icon-border.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "\nUse rarely for icons and borders representing semi-urgent information, such as the icons in warning section messages.\n\nAlso use for yellow icons or borders when there is no meaning tied to the color, such as file type icons.\n"
      },
      "value": "Y500"
    },
    "name": "color.iconBorder.warning",
    "path": [
      "color",
      "iconBorder",
      "warning"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons and borders representing positive information, such as the icons in success section messages or the borders on validated text fields.\n\nAlso use for green icons or borders when there is no meaning tied to the color, such as file type icons.\n"
    },
    "value": "#2ABB7F",
    "filePath": "src/tokens/atlassian-dark/color/icon-border.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "\nUse rarely for icons and borders representing positive information, such as the icons in success section messages or the borders on validated text fields.\n\nAlso use for green icons or borders when there is no meaning tied to the color, such as file type icons.\n"
      },
      "value": "G500"
    },
    "name": "color.iconBorder.success",
    "path": [
      "color",
      "iconBorder",
      "success"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons and borders representing new information, such as the icons in discovery section mesages or the borders in onboarding spotlights.\n\nAlso use for purple icons or borders when there is no meaning tied to the color, such as file type icons.\n"
    },
    "value": "#8F7EE7",
    "filePath": "src/tokens/atlassian-dark/color/icon-border.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "\nUse rarely for icons and borders representing new information, such as the icons in discovery section mesages or the borders in onboarding spotlights.\n\nAlso use for purple icons or borders when there is no meaning tied to the color, such as file type icons.\n"
      },
      "value": "P500"
    },
    "name": "color.iconBorder.discovery",
    "path": [
      "color",
      "iconBorder",
      "discovery"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as a background overlay for elements in a hover state when their background color cannot change, such as avatars."
    },
    "value": "#BFDBF847",
    "filePath": "src/tokens/atlassian-dark/color/overlay.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use as a background overlay for elements in a hover state when their background color cannot change, such as avatars."
      },
      "value": "DN400A"
    },
    "name": "color.overlay.hover",
    "path": [
      "color",
      "overlay",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as a background overlay for elements in a pressed state when their background color cannot change, such as avatars."
    },
    "value": "#A9C5DF7A",
    "filePath": "src/tokens/atlassian-dark/color/overlay.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use as a background overlay for elements in a pressed state when their background color cannot change, such as avatars."
      },
      "value": "DN500A"
    },
    "name": "color.overlay.pressed",
    "path": [
      "color",
      "overlay",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for text, icons, borders, or other visual indicators in selected states"
    },
    "value": "#579DFF",
    "filePath": "src/tokens/atlassian-dark/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for text, icons, borders, or other visual indicators in selected states"
      },
      "value": "B400"
    },
    "name": "color.text.selected",
    "path": [
      "color",
      "text",
      "selected"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for primary text, such as body copy, sentence case headers, and buttons"
    },
    "value": "#C7D1DB",
    "filePath": "src/tokens/atlassian-dark/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for primary text, such as body copy, sentence case headers, and buttons"
      },
      "value": "DN1000"
    },
    "name": "color.text.highEmphasis",
    "path": [
      "color",
      "text",
      "highEmphasis"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse for secondary text, such navigation, subtle button links, input field labels, and all caps subheadings.\n\nUse for icon-only buttons, or icons paired with text.highEmphasis text\n      "
    },
    "value": "#9FADBC",
    "filePath": "src/tokens/atlassian-dark/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "\nUse for secondary text, such navigation, subtle button links, input field labels, and all caps subheadings.\n\nUse for icon-only buttons, or icons paired with text.highEmphasis text\n      "
      },
      "value": "DN800"
    },
    "name": "color.text.mediumEmphasis",
    "path": [
      "color",
      "text",
      "mediumEmphasis"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse for tertiary text, such as meta-data, breadcrumbs, input field placeholder and helper text.\n\nUse for icons that are paired with text.medEmphasis text"
    },
    "value": "#8696A7",
    "filePath": "src/tokens/atlassian-dark/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "\nUse for tertiary text, such as meta-data, breadcrumbs, input field placeholder and helper text.\n\nUse for icons that are paired with text.medEmphasis text"
      },
      "value": "DN700"
    },
    "name": "color.text.lowEmphasis",
    "path": [
      "color",
      "text",
      "lowEmphasis"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for text and icons when on bold backgrounds"
    },
    "value": "#161A1D",
    "filePath": "src/tokens/atlassian-dark/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for text and icons when on bold backgrounds"
      },
      "value": "DN0"
    },
    "name": "color.text.onBold",
    "path": [
      "color",
      "text",
      "onBold"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for text and icons when on bold warning backgrounds"
    },
    "value": "#161A1D",
    "filePath": "src/tokens/atlassian-dark/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for text and icons when on bold warning backgrounds"
      },
      "value": "DN0"
    },
    "name": "color.text.onBoldWarning",
    "path": [
      "color",
      "text",
      "onBoldWarning"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for links in a resting or hover state. Add an underline for hover states"
    },
    "value": "#579DFF",
    "filePath": "src/tokens/atlassian-dark/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for links in a resting or hover state. Add an underline for hover states"
      },
      "value": "B400"
    },
    "name": "color.text.link.resting",
    "path": [
      "color",
      "text",
      "link",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for links in a pressed state"
    },
    "value": "#85B8FF",
    "filePath": "src/tokens/atlassian-dark/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for links in a pressed state"
      },
      "value": "B300"
    },
    "name": "color.text.link.pressed",
    "path": [
      "color",
      "text",
      "link",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for text on subtle brand backgrounds, such as in progress lozenges, or on subtle blue accent backgrounds, such as colored tags."
    },
    "value": "#85B8FF",
    "filePath": "src/tokens/atlassian-dark/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use rarely for text on subtle brand backgrounds, such as in progress lozenges, or on subtle blue accent backgrounds, such as colored tags."
      },
      "value": "B300"
    },
    "name": "color.text.brand",
    "path": [
      "color",
      "text",
      "brand"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for text on subtle warning backgrounds, such as in lozenges, or text on subtle warning backgrounds, such as in moved lozenges"
    },
    "value": "#F5CD47",
    "filePath": "src/tokens/atlassian-dark/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use rarely for text on subtle warning backgrounds, such as in lozenges, or text on subtle warning backgrounds, such as in moved lozenges"
      },
      "value": "Y300"
    },
    "name": "color.text.warning",
    "path": [
      "color",
      "text",
      "warning"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for critical text, such as input field error messaging, or text on subtle danger backgrounds, such as in removed lozenges, or text on subtle red accent backgrounds, such as colored tags."
    },
    "value": "#FF9C8F",
    "filePath": "src/tokens/atlassian-dark/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use rarely for critical text, such as input field error messaging, or text on subtle danger backgrounds, such as in removed lozenges, or text on subtle red accent backgrounds, such as colored tags."
      },
      "value": "R300"
    },
    "name": "color.text.danger",
    "path": [
      "color",
      "text",
      "danger"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for positive text, such as input field success messaging, or text on subtle success backgrounds, such as in success lozenges, or text on subtle green accent backgrounds, such as colored tags."
    },
    "value": "#7EE2B8",
    "filePath": "src/tokens/atlassian-dark/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use rarely for positive text, such as input field success messaging, or text on subtle success backgrounds, such as in success lozenges, or text on subtle green accent backgrounds, such as colored tags."
      },
      "value": "G300"
    },
    "name": "color.text.success",
    "path": [
      "color",
      "text",
      "success"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for text on subtle discovery backgrounds, such as in new lozenges, or text on subtle purple accent backgrounds, such as colored tags."
    },
    "value": "#B8ACF6",
    "filePath": "src/tokens/atlassian-dark/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use rarely for text on subtle discovery backgrounds, such as in new lozenges, or text on subtle purple accent backgrounds, such as colored tags."
      },
      "value": "P300"
    },
    "name": "color.text.discovery",
    "path": [
      "color",
      "text",
      "discovery"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for text and icons in disabled states"
    },
    "value": "#5C6C7A",
    "filePath": "src/tokens/atlassian-dark/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for text and icons in disabled states"
      },
      "value": "DN500"
    },
    "name": "color.text.disabled",
    "path": [
      "color",
      "text",
      "disabled"
    ]
  },
  {
    "attributes": {
      "group": "shadow",
      "state": "active",
      "description": "\nUse for the box shadow of raised card elements, such as Jira cards on a Kanban board.\n\nCombine with background.overlay"
    },
    "value": [
      {
        "radius": 1,
        "offset": {
          "x": 0,
          "y": 1
        },
        "color": "#03040442",
        "opacity": 0.5
      },
      {
        "radius": 1,
        "offset": {
          "x": 0,
          "y": 0
        },
        "color": "#03040442",
        "opacity": 0.5
      }
    ],
    "filePath": "src/tokens/atlassian-dark/shadow/shadow.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "shadow",
        "state": "active",
        "description": "\nUse for the box shadow of raised card elements, such as Jira cards on a Kanban board.\n\nCombine with background.overlay"
      },
      "value": [
        {
          "radius": 1,
          "offset": {
            "x": 0,
            "y": 1
          },
          "color": "DN-100A",
          "opacity": 0.5
        },
        {
          "radius": 1,
          "offset": {
            "x": 0,
            "y": 0
          },
          "color": "DN-100A",
          "opacity": 0.5
        }
      ]
    },
    "name": "shadow.card",
    "path": [
      "shadow",
      "card"
    ]
  },
  {
    "attributes": {
      "group": "shadow",
      "state": "active",
      "description": "\nUse for the box shadow of overlay elements, such as modals, dropdown menus, flags, and inline dialogs (i.e. elements that sit on top of the UI).\n\nAlso use for the box shadow of raised cards in a dragged state.\n\nCombine with background.overlay"
    },
    "value": [
      {
        "radius": 0,
        "spread": 1,
        "color": "#BCD6F00A",
        "offset": {
          "x": 0,
          "y": 0
        },
        "opacity": 0.04,
        "inset": true
      },
      {
        "radius": 12,
        "offset": {
          "x": 0,
          "y": 8
        },
        "color": "#03040442",
        "opacity": 0.36
      },
      {
        "radius": 1,
        "offset": {
          "x": 0,
          "y": 0
        },
        "color": "#03040442",
        "opacity": 0.5
      }
    ],
    "filePath": "src/tokens/atlassian-dark/shadow/shadow.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "shadow",
        "state": "active",
        "description": "\nUse for the box shadow of overlay elements, such as modals, dropdown menus, flags, and inline dialogs (i.e. elements that sit on top of the UI).\n\nAlso use for the box shadow of raised cards in a dragged state.\n\nCombine with background.overlay"
      },
      "value": [
        {
          "radius": 0,
          "spread": 1,
          "color": "DN100A",
          "offset": {
            "x": 0,
            "y": 0
          },
          "opacity": 0.04,
          "inset": true
        },
        {
          "radius": 12,
          "offset": {
            "x": 0,
            "y": 8
          },
          "color": "DN-100A",
          "opacity": 0.36
        },
        {
          "radius": 1,
          "offset": {
            "x": 0,
            "y": 0
          },
          "color": "DN-100A",
          "opacity": 0.5
        }
      ]
    },
    "name": "shadow.overlay",
    "path": [
      "shadow",
      "overlay"
    ]
  },
  {
    "attributes": {
      "group": "raw",
      "state": "active",
      "description": "Transparent token used for backwards compatibility between new and old theming solutions"
    },
    "value": "transparent",
    "filePath": "src/tokens/atlassian-dark/utility/utility.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "raw",
        "state": "active",
        "description": "Transparent token used for backwards compatibility between new and old theming solutions"
      },
      "value": "transparent"
    },
    "name": "utility.UNSAFE_util.transparent",
    "path": [
      "utility",
      "UNSAFE_util",
      "transparent"
    ]
  },
  {
    "attributes": {
      "group": "raw",
      "state": "active",
      "description": "Used as a placeholder when a suitable token does not exist"
    },
    "value": "#FA11F2",
    "filePath": "src/tokens/atlassian-dark/utility/utility.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "raw",
        "state": "active",
        "description": "Used as a placeholder when a suitable token does not exist"
      },
      "value": "#FA11F2"
    },
    "name": "utility.UNSAFE_util.MISSING_TOKEN",
    "path": [
      "utility",
      "UNSAFE_util",
      "MISSING_TOKEN"
    ]
  }
];

export default tokens;
