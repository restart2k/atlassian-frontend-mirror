
// THIS IS AN AUTO-GENERATED FILE DO NOT MODIFY DIRECTLY
// Re-generate by running `yarn build tokens`.

const tokens = [
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for blue backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#579DFF",
    "filePath": "src/tokens/atlassian-light/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for blue backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
      },
      "value": "B400"
    },
    "name": "color.accent.boldBlue",
    "path": [
      "color",
      "accent",
      "boldBlue"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for green backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#4BCE97",
    "filePath": "src/tokens/atlassian-light/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for green backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
      },
      "value": "G400"
    },
    "name": "color.accent.boldGreen",
    "path": [
      "color",
      "accent",
      "boldGreen"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for orange backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#FAA53D",
    "filePath": "src/tokens/atlassian-light/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for orange backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
      },
      "value": "O400"
    },
    "name": "color.accent.boldOrange",
    "path": [
      "color",
      "accent",
      "boldOrange"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for purple backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#9F8FEF",
    "filePath": "src/tokens/atlassian-light/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for purple backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
      },
      "value": "P400"
    },
    "name": "color.accent.boldPurple",
    "path": [
      "color",
      "accent",
      "boldPurple"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for red backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#F87462",
    "filePath": "src/tokens/atlassian-light/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for red backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
      },
      "value": "R400"
    },
    "name": "color.accent.boldRed",
    "path": [
      "color",
      "accent",
      "boldRed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for teal backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#60C6D2",
    "filePath": "src/tokens/atlassian-light/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for teal backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
      },
      "value": "T400"
    },
    "name": "color.accent.boldTeal",
    "path": [
      "color",
      "accent",
      "boldTeal"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for blue subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#CCE0FF",
    "filePath": "src/tokens/atlassian-light/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for blue subdued backgrounds when there is no meaning tied to the color, such as colored tags."
      },
      "value": "B200"
    },
    "name": "color.accent.subtleBlue",
    "path": [
      "color",
      "accent",
      "subtleBlue"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for green subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#BAF3DB",
    "filePath": "src/tokens/atlassian-light/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for green subdued backgrounds when there is no meaning tied to the color, such as colored tags."
      },
      "value": "G200"
    },
    "name": "color.accent.subtleGreen",
    "path": [
      "color",
      "accent",
      "subtleGreen"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for magenta subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#FDD0EC",
    "filePath": "src/tokens/atlassian-light/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for magenta subdued backgrounds when there is no meaning tied to the color, such as colored tags."
      },
      "value": "M200"
    },
    "name": "color.accent.subtleMagenta",
    "path": [
      "color",
      "accent",
      "subtleMagenta"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for orange subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#FFE2BD",
    "filePath": "src/tokens/atlassian-light/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for orange subdued backgrounds when there is no meaning tied to the color, such as colored tags."
      },
      "value": "O200"
    },
    "name": "color.accent.subtleOrange",
    "path": [
      "color",
      "accent",
      "subtleOrange"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for purple subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#DFD8FD",
    "filePath": "src/tokens/atlassian-light/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for purple subdued backgrounds when there is no meaning tied to the color, such as colored tags."
      },
      "value": "P200"
    },
    "name": "color.accent.subtlePurple",
    "path": [
      "color",
      "accent",
      "subtlePurple"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for red subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#FFD2CC",
    "filePath": "src/tokens/atlassian-light/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for red subdued backgrounds when there is no meaning tied to the color, such as colored tags."
      },
      "value": "R200"
    },
    "name": "color.accent.subtleRed",
    "path": [
      "color",
      "accent",
      "subtleRed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for teal subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#C1F0F5",
    "filePath": "src/tokens/atlassian-light/color/accent.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for teal subdued backgrounds when there is no meaning tied to the color, such as colored tags."
      },
      "value": "T200"
    },
    "name": "color.accent.subtleTeal",
    "path": [
      "color",
      "accent",
      "subtleTeal"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as a secondary background for the UI"
    },
    "value": "#091E4208",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use as a secondary background for the UI"
      },
      "value": "N100A"
    },
    "name": "color.background.sunken",
    "path": [
      "color",
      "background",
      "sunken"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as the primary background for the UI"
    },
    "value": "#FFFFFF",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use as the primary background for the UI"
      },
      "value": "N0"
    },
    "name": "color.background.default",
    "path": [
      "color",
      "background",
      "default"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for the background of raised cards, such as Jira cards on a Kanban board.\nCombine with shadow.card."
    },
    "value": "#FFFFFF",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for the background of raised cards, such as Jira cards on a Kanban board.\nCombine with shadow.card."
      },
      "value": "N0"
    },
    "name": "color.background.card",
    "path": [
      "color",
      "background",
      "card"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse for the background of overlay elements, such as modals, dropdown menus, flags, and inline dialogs (i.e. elements that sit on top of the UI).\n\nAlso use for the background of raised cards in a dragged state.\n\nCombine with shadow.overlay."
    },
    "value": "#FFFFFF",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "\nUse for the background of overlay elements, such as modals, dropdown menus, flags, and inline dialogs (i.e. elements that sit on top of the UI).\n\nAlso use for the background of raised cards in a dragged state.\n\nCombine with shadow.overlay."
      },
      "value": "N0"
    },
    "name": "color.background.overlay",
    "path": [
      "color",
      "background",
      "overlay"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for backgrounds of elements in a selected state"
    },
    "value": "#E9F2FF",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for backgrounds of elements in a selected state"
      },
      "value": "B100"
    },
    "name": "color.background.selected.resting",
    "path": [
      "color",
      "background",
      "selected",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.selected"
    },
    "value": "#CCE0FF",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state of background.selected"
      },
      "value": "B200"
    },
    "name": "color.background.selected.hover",
    "path": [
      "color",
      "background",
      "selected",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.selected"
    },
    "value": "#85B8FF",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state of background.selected"
      },
      "value": "B300"
    },
    "name": "color.background.selected.pressed",
    "path": [
      "color",
      "background",
      "selected",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for the screen overlay that appears with modal dialogs"
    },
    "value": "#091E427A",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for the screen overlay that appears with modal dialogs"
      },
      "value": "N500A"
    },
    "name": "color.background.blanket",
    "path": [
      "color",
      "background",
      "blanket"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for backgrounds of elements in a disabled state"
    },
    "value": "#091E420F",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for backgrounds of elements in a disabled state"
      },
      "value": "N200A"
    },
    "name": "color.background.disabled",
    "path": [
      "color",
      "background",
      "disabled"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like primary buttons and bold in progress lozenges."
    },
    "value": "#0C66E4",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "A vibrant background for small UI elements like primary buttons and bold in progress lozenges."
      },
      "value": "B700"
    },
    "name": "color.background.boldBrand.resting",
    "path": [
      "color",
      "background",
      "boldBrand",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldBrand"
    },
    "value": "#0055CC",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state of background.boldBrand"
      },
      "value": "B800"
    },
    "name": "color.background.boldBrand.hover",
    "path": [
      "color",
      "background",
      "boldBrand",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldBrand"
    },
    "value": "#09326C",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state of background.boldBrand"
      },
      "value": "B900"
    },
    "name": "color.background.boldBrand.pressed",
    "path": [
      "color",
      "background",
      "boldBrand",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like information section messages and in progress lozenges."
    },
    "value": "#E9F2FF",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for subdued backgrounds of UI elements like information section messages and in progress lozenges."
      },
      "value": "B100"
    },
    "name": "color.background.subtleBrand.resting",
    "path": [
      "color",
      "background",
      "subtleBrand",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleBrand"
    },
    "value": "#CCE0FF",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state for background.subtleBrand"
      },
      "value": "B200"
    },
    "name": "color.background.subtleBrand.hover",
    "path": [
      "color",
      "background",
      "subtleBrand",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleBrand"
    },
    "value": "#85B8FF",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state for background.subtleBrand"
      },
      "value": "B300"
    },
    "name": "color.background.subtleBrand.pressed",
    "path": [
      "color",
      "background",
      "subtleBrand",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like danger buttons and bold removed lozenges."
    },
    "value": "#CA3521",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "A vibrant background for small UI elements like danger buttons and bold removed lozenges."
      },
      "value": "R700"
    },
    "name": "color.background.boldDanger.resting",
    "path": [
      "color",
      "background",
      "boldDanger",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldDanger"
    },
    "value": "#AE2A19",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state of background.boldDanger"
      },
      "value": "R800"
    },
    "name": "color.background.boldDanger.hover",
    "path": [
      "color",
      "background",
      "boldDanger",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldDanger"
    },
    "value": "#601E16",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state of background.boldDanger"
      },
      "value": "R900"
    },
    "name": "color.background.boldDanger.pressed",
    "path": [
      "color",
      "background",
      "boldDanger",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like error section messages and removed lozenges."
    },
    "value": "#FFEDEB",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for subdued backgrounds of UI elements like error section messages and removed lozenges."
      },
      "value": "R100"
    },
    "name": "color.background.subtleDanger.resting",
    "path": [
      "color",
      "background",
      "subtleDanger",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleDanger"
    },
    "value": "#FFD2CC",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state for background.subtleDanger"
      },
      "value": "R200"
    },
    "name": "color.background.subtleDanger.hover",
    "path": [
      "color",
      "background",
      "subtleDanger",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleDanger"
    },
    "value": "#FF9C8F",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state for background.subtleDanger"
      },
      "value": "R300"
    },
    "name": "color.background.subtleDanger.pressed",
    "path": [
      "color",
      "background",
      "subtleDanger",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like warning buttons and bold moved lozenges."
    },
    "value": "#E2B203",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "A vibrant background for small UI elements like warning buttons and bold moved lozenges."
      },
      "value": "Y400"
    },
    "name": "color.background.boldWarning.resting",
    "path": [
      "color",
      "background",
      "boldWarning",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldWarning"
    },
    "value": "#CF9F02",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state of background.boldWarning"
      },
      "value": "Y500"
    },
    "name": "color.background.boldWarning.hover",
    "path": [
      "color",
      "background",
      "boldWarning",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldWarning"
    },
    "value": "#B38600",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state of background.boldWarning"
      },
      "value": "Y600"
    },
    "name": "color.background.boldWarning.pressed",
    "path": [
      "color",
      "background",
      "boldWarning",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like warning section messages and moved lozenges."
    },
    "value": "#FFF7D6",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for subdued backgrounds of UI elements like warning section messages and moved lozenges."
      },
      "value": "Y100"
    },
    "name": "color.background.subtleWarning.resting",
    "path": [
      "color",
      "background",
      "subtleWarning",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleWarning"
    },
    "value": "#F8E6A0",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state for background.subtleWarning"
      },
      "value": "Y200"
    },
    "name": "color.background.subtleWarning.hover",
    "path": [
      "color",
      "background",
      "subtleWarning",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleWarning"
    },
    "value": "#F5CD47",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state for background.subtleWarning"
      },
      "value": "Y300"
    },
    "name": "color.background.subtleWarning.pressed",
    "path": [
      "color",
      "background",
      "subtleWarning",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like checked toggles and bold success lozenges."
    },
    "value": "#1F845A",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "A vibrant background for small UI elements like checked toggles and bold success lozenges."
      },
      "value": "G700"
    },
    "name": "color.background.boldSuccess.resting",
    "path": [
      "color",
      "background",
      "boldSuccess",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldSuccess"
    },
    "value": "#216E4E",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state of background.boldSuccess"
      },
      "value": "G800"
    },
    "name": "color.background.boldSuccess.hover",
    "path": [
      "color",
      "background",
      "boldSuccess",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldSuccess"
    },
    "value": "#164B35",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state of background.boldSuccess"
      },
      "value": "G900"
    },
    "name": "color.background.boldSuccess.pressed",
    "path": [
      "color",
      "background",
      "boldSuccess",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like success section messages and success lozenges. "
    },
    "value": "#DFFCF0",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for subdued backgrounds of UI elements like success section messages and success lozenges. "
      },
      "value": "G100"
    },
    "name": "color.background.subtleSuccess.resting",
    "path": [
      "color",
      "background",
      "subtleSuccess",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleSuccess"
    },
    "value": "#BAF3DB",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state for background.subtleSuccess"
      },
      "value": "G200"
    },
    "name": "color.background.subtleSuccess.hover",
    "path": [
      "color",
      "background",
      "subtleSuccess",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleSuccess"
    },
    "value": "#7EE2B8",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state for background.subtleSuccess"
      },
      "value": "G300"
    },
    "name": "color.background.subtleSuccess.pressed",
    "path": [
      "color",
      "background",
      "subtleSuccess",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like onboarding buttons and bold new lozenges."
    },
    "value": "#6E5DC6",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "A vibrant background for small UI elements like onboarding buttons and bold new lozenges."
      },
      "value": "P700"
    },
    "name": "color.background.boldDiscovery.resting",
    "path": [
      "color",
      "background",
      "boldDiscovery",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldDiscovery"
    },
    "value": "#5E4DB2",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state of background.boldDiscovery"
      },
      "value": "P800"
    },
    "name": "color.background.boldDiscovery.hover",
    "path": [
      "color",
      "background",
      "boldDiscovery",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldDiscovery"
    },
    "value": "#352C63",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state of background.boldDiscovery"
      },
      "value": "P900"
    },
    "name": "color.background.boldDiscovery.pressed",
    "path": [
      "color",
      "background",
      "boldDiscovery",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like discovery section messages and new lozenges."
    },
    "value": "#F3F0FF",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for subdued backgrounds of UI elements like discovery section messages and new lozenges."
      },
      "value": "P100"
    },
    "name": "color.background.subtleDiscovery.resting",
    "path": [
      "color",
      "background",
      "subtleDiscovery",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleDiscovery"
    },
    "value": "#DFD8FD",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state for background.subtleDiscovery"
      },
      "value": "P200"
    },
    "name": "color.background.subtleDiscovery.hover",
    "path": [
      "color",
      "background",
      "subtleDiscovery",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleDiscovery"
    },
    "value": "#B8ACF6",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state for background.subtleDiscovery"
      },
      "value": "P300"
    },
    "name": "color.background.subtleDiscovery.pressed",
    "path": [
      "color",
      "background",
      "subtleDiscovery",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like unchecked toggles and bold default lozenges."
    },
    "value": "#44546F",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "A vibrant background for small UI elements like unchecked toggles and bold default lozenges."
      },
      "value": "N800"
    },
    "name": "color.background.boldNeutral.resting",
    "path": [
      "color",
      "background",
      "boldNeutral",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldNeutral"
    },
    "value": "#2C3E5D",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state of background.boldNeutral"
      },
      "value": "N900"
    },
    "name": "color.background.boldNeutral.hover",
    "path": [
      "color",
      "background",
      "boldNeutral",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldNeutral"
    },
    "value": "#172B4D",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state of background.boldNeutral"
      },
      "value": "N1000"
    },
    "name": "color.background.boldNeutral.pressed",
    "path": [
      "color",
      "background",
      "boldNeutral",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for UIs that don’t have a default background, such as menu items or subtle buttons."
    },
    "value": "#091E420F",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state for UIs that don’t have a default background, such as menu items or subtle buttons."
      },
      "value": "N200A"
    },
    "name": "color.background.transparentNeutral.hover",
    "path": [
      "color",
      "background",
      "transparentNeutral",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for UIs that don’t have a default background, such as menu items or subtle buttons."
    },
    "value": "#091E4224",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state for UIs that don’t have a default background, such as menu items or subtle buttons."
      },
      "value": "N300A"
    },
    "name": "color.background.transparentNeutral.pressed",
    "path": [
      "color",
      "background",
      "transparentNeutral",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as the default background of UI elements like buttons, lozenges, and tags."
    },
    "value": "#091E420F",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use as the default background of UI elements like buttons, lozenges, and tags."
      },
      "value": "N200A"
    },
    "name": "color.background.subtleNeutral.resting",
    "path": [
      "color",
      "background",
      "subtleNeutral",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleNeutral"
    },
    "value": "#091E4224",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state for background.subtleNeutral"
      },
      "value": "N300A"
    },
    "name": "color.background.subtleNeutral.hover",
    "path": [
      "color",
      "background",
      "subtleNeutral",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleNeutral"
    },
    "value": "#091E424F",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state for background.subtleNeutral"
      },
      "value": "N400A"
    },
    "name": "color.background.subtleNeutral.pressed",
    "path": [
      "color",
      "background",
      "subtleNeutral",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleBorderedNeutral"
    },
    "value": "#091E4208",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Hover state for background.subtleBorderedNeutral"
      },
      "value": "N100A"
    },
    "name": "color.background.subtleBorderedNeutral.resting",
    "path": [
      "color",
      "background",
      "subtleBorderedNeutral",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleBorderedNeutral"
    },
    "value": "#091E420F",
    "filePath": "src/tokens/atlassian-light/color/background.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Pressed state for background.subtleBorderedNeutral"
      },
      "value": "N200A"
    },
    "name": "color.background.subtleBorderedNeutral.pressed",
    "path": [
      "color",
      "background",
      "subtleBorderedNeutral",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for focus rings of elements in a focus state"
    },
    "value": "#388BFF",
    "filePath": "src/tokens/atlassian-light/color/border.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for focus rings of elements in a focus state"
      },
      "value": "B500"
    },
    "name": "color.border.focus",
    "path": [
      "color",
      "border",
      "focus"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use to create borders around UI elements such as text fields, checkboxes, and radio buttons, or to visually group or separate UI elements, such as flat cards or side panel dividers"
    },
    "value": "#091E4224",
    "filePath": "src/tokens/atlassian-light/color/border.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use to create borders around UI elements such as text fields, checkboxes, and radio buttons, or to visually group or separate UI elements, such as flat cards or side panel dividers"
      },
      "value": "N300A"
    },
    "name": "color.border.neutral",
    "path": [
      "color",
      "border",
      "neutral"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons or borders representing brand, in-progress, or information, such as the icons in information sections messages.\n\nAlso use for blue icons or borders when there is no meaning tied to the color, such as file type icons."
    },
    "value": "#1D7AFC",
    "filePath": "src/tokens/atlassian-light/color/icon-border.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "\nUse rarely for icons or borders representing brand, in-progress, or information, such as the icons in information sections messages.\n\nAlso use for blue icons or borders when there is no meaning tied to the color, such as file type icons."
      },
      "value": "B600"
    },
    "name": "color.iconBorder.brand",
    "path": [
      "color",
      "iconBorder",
      "brand"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons and borders representing critical information, such the icons in error section messages or the borders on invalid text fields.\n\nAlso use for red icons or borders when there is no meaning tied to the color, such as file type icons."
    },
    "value": "#E34935",
    "filePath": "src/tokens/atlassian-light/color/icon-border.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "\nUse rarely for icons and borders representing critical information, such the icons in error section messages or the borders on invalid text fields.\n\nAlso use for red icons or borders when there is no meaning tied to the color, such as file type icons."
      },
      "value": "R600"
    },
    "name": "color.iconBorder.danger",
    "path": [
      "color",
      "iconBorder",
      "danger"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons and borders representing semi-urgent information, such as the icons in warning section messages.\n\nAlso use for yellow icons or borders when there is no meaning tied to the color, such as file type icons.\n"
    },
    "value": "#D97008",
    "filePath": "src/tokens/atlassian-light/color/icon-border.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "\nUse rarely for icons and borders representing semi-urgent information, such as the icons in warning section messages.\n\nAlso use for yellow icons or borders when there is no meaning tied to the color, such as file type icons.\n"
      },
      "value": "O600"
    },
    "name": "color.iconBorder.warning",
    "path": [
      "color",
      "iconBorder",
      "warning"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons and borders representing positive information, such as the icons in success section messages or the borders on validated text fields.\n\nAlso use for green icons or borders when there is no meaning tied to the color, such as file type icons.\n"
    },
    "value": "#22A06B",
    "filePath": "src/tokens/atlassian-light/color/icon-border.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "\nUse rarely for icons and borders representing positive information, such as the icons in success section messages or the borders on validated text fields.\n\nAlso use for green icons or borders when there is no meaning tied to the color, such as file type icons.\n"
      },
      "value": "G600"
    },
    "name": "color.iconBorder.success",
    "path": [
      "color",
      "iconBorder",
      "success"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons and borders representing new information, such as the icons in discovery section mesages or the borders in onboarding spotlights.\n\nAlso use for purple icons or borders when there is no meaning tied to the color, such as file type icons.\n"
    },
    "value": "#8270DB",
    "filePath": "src/tokens/atlassian-light/color/icon-border.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "\nUse rarely for icons and borders representing new information, such as the icons in discovery section mesages or the borders in onboarding spotlights.\n\nAlso use for purple icons or borders when there is no meaning tied to the color, such as file type icons.\n"
      },
      "value": "P600"
    },
    "name": "color.iconBorder.discovery",
    "path": [
      "color",
      "iconBorder",
      "discovery"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as a background overlay for elements in a hover state when their background color cannot change, such as avatars."
    },
    "value": "#091E424F",
    "filePath": "src/tokens/atlassian-light/color/overlay.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use as a background overlay for elements in a hover state when their background color cannot change, such as avatars."
      },
      "value": "N400A"
    },
    "name": "color.overlay.hover",
    "path": [
      "color",
      "overlay",
      "hover"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as a background overlay for elements in a pressed state when their background color cannot change, such as avatars."
    },
    "value": "#091E427A",
    "filePath": "src/tokens/atlassian-light/color/overlay.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use as a background overlay for elements in a pressed state when their background color cannot change, such as avatars."
      },
      "value": "N500A"
    },
    "name": "color.overlay.pressed",
    "path": [
      "color",
      "overlay",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for text, icons, borders, or other visual indicators in selected states"
    },
    "value": "#0C66E4",
    "filePath": "src/tokens/atlassian-light/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for text, icons, borders, or other visual indicators in selected states"
      },
      "value": "B700"
    },
    "name": "color.text.selected",
    "path": [
      "color",
      "text",
      "selected"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for primary text, such as body copy, sentence case headers, and buttons"
    },
    "value": "#172B4D",
    "filePath": "src/tokens/atlassian-light/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for primary text, such as body copy, sentence case headers, and buttons"
      },
      "value": "N1000"
    },
    "name": "color.text.highEmphasis",
    "path": [
      "color",
      "text",
      "highEmphasis"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse for secondary text, such navigation, subtle button links, input field labels, and all caps subheadings.\n\nUse for icon-only buttons, or icons paired with text.highEmphasis text\n      "
    },
    "value": "#44546F",
    "filePath": "src/tokens/atlassian-light/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "\nUse for secondary text, such navigation, subtle button links, input field labels, and all caps subheadings.\n\nUse for icon-only buttons, or icons paired with text.highEmphasis text\n      "
      },
      "value": "N800"
    },
    "name": "color.text.mediumEmphasis",
    "path": [
      "color",
      "text",
      "mediumEmphasis"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse for tertiary text, such as meta-data, breadcrumbs, input field placeholder and helper text.\n\nUse for icons that are paired with text.medEmphasis text"
    },
    "value": "#626F86",
    "filePath": "src/tokens/atlassian-light/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "\nUse for tertiary text, such as meta-data, breadcrumbs, input field placeholder and helper text.\n\nUse for icons that are paired with text.medEmphasis text"
      },
      "value": "N700"
    },
    "name": "color.text.lowEmphasis",
    "path": [
      "color",
      "text",
      "lowEmphasis"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for text and icons when on bold backgrounds"
    },
    "value": "#FFFFFF",
    "filePath": "src/tokens/atlassian-light/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for text and icons when on bold backgrounds"
      },
      "value": "N0"
    },
    "name": "color.text.onBold",
    "path": [
      "color",
      "text",
      "onBold"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for text and icons when on bold warning backgrounds"
    },
    "value": "#172B4D",
    "filePath": "src/tokens/atlassian-light/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for text and icons when on bold warning backgrounds"
      },
      "value": "N1000"
    },
    "name": "color.text.onBoldWarning",
    "path": [
      "color",
      "text",
      "onBoldWarning"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for links in a resting or hover state. Add an underline for hover states"
    },
    "value": "#0C66E4",
    "filePath": "src/tokens/atlassian-light/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for links in a resting or hover state. Add an underline for hover states"
      },
      "value": "B700"
    },
    "name": "color.text.link.resting",
    "path": [
      "color",
      "text",
      "link",
      "resting"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for links in a pressed state"
    },
    "value": "#0055CC",
    "filePath": "src/tokens/atlassian-light/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for links in a pressed state"
      },
      "value": "B800"
    },
    "name": "color.text.link.pressed",
    "path": [
      "color",
      "text",
      "link",
      "pressed"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for text on subtle brand backgrounds, such as in progress lozenges, or on subtle blue accent backgrounds, such as colored tags."
    },
    "value": "#0055CC",
    "filePath": "src/tokens/atlassian-light/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use rarely for text on subtle brand backgrounds, such as in progress lozenges, or on subtle blue accent backgrounds, such as colored tags."
      },
      "value": "B800"
    },
    "name": "color.text.brand",
    "path": [
      "color",
      "text",
      "brand"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for text on subtle warning backgrounds, such as in lozenges, or text on subtle warning backgrounds, such as in moved lozenges"
    },
    "value": "#974F0C",
    "filePath": "src/tokens/atlassian-light/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use rarely for text on subtle warning backgrounds, such as in lozenges, or text on subtle warning backgrounds, such as in moved lozenges"
      },
      "value": "O800"
    },
    "name": "color.text.warning",
    "path": [
      "color",
      "text",
      "warning"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for critical text, such as input field error messaging, or text on subtle danger backgrounds, such as in removed lozenges, or text on subtle red accent backgrounds, such as colored tags."
    },
    "value": "#AE2A19",
    "filePath": "src/tokens/atlassian-light/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use rarely for critical text, such as input field error messaging, or text on subtle danger backgrounds, such as in removed lozenges, or text on subtle red accent backgrounds, such as colored tags."
      },
      "value": "R800"
    },
    "name": "color.text.danger",
    "path": [
      "color",
      "text",
      "danger"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for positive text, such as input field success messaging, or text on subtle success backgrounds, such as in success lozenges, or text on subtle green accent backgrounds, such as colored tags."
    },
    "value": "#216E4E",
    "filePath": "src/tokens/atlassian-light/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use rarely for positive text, such as input field success messaging, or text on subtle success backgrounds, such as in success lozenges, or text on subtle green accent backgrounds, such as colored tags."
      },
      "value": "G800"
    },
    "name": "color.text.success",
    "path": [
      "color",
      "text",
      "success"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for text on subtle discovery backgrounds, such as in new lozenges, or text on subtle purple accent backgrounds, such as colored tags."
    },
    "value": "#5E4DB2",
    "filePath": "src/tokens/atlassian-light/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use rarely for text on subtle discovery backgrounds, such as in new lozenges, or text on subtle purple accent backgrounds, such as colored tags."
      },
      "value": "P800"
    },
    "name": "color.text.discovery",
    "path": [
      "color",
      "text",
      "discovery"
    ]
  },
  {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for text and icons in disabled states"
    },
    "value": "#8993A5",
    "filePath": "src/tokens/atlassian-light/color/text.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "paint",
        "state": "active",
        "description": "Use for text and icons in disabled states"
      },
      "value": "N500"
    },
    "name": "color.text.disabled",
    "path": [
      "color",
      "text",
      "disabled"
    ]
  },
  {
    "attributes": {
      "group": "shadow",
      "state": "active",
      "description": "\nUse for the box shadow of raised card elements, such as Jira cards on a Kanban board.\n\nCombine with background.overlay"
    },
    "value": [
      {
        "radius": 1,
        "offset": {
          "x": 0,
          "y": 1
        },
        "color": "#091E42",
        "opacity": 0.25
      },
      {
        "radius": 1,
        "offset": {
          "x": 0,
          "y": 0
        },
        "color": "#091E42",
        "opacity": 0.31
      }
    ],
    "filePath": "src/tokens/atlassian-light/shadow/shadow.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "shadow",
        "state": "active",
        "description": "\nUse for the box shadow of raised card elements, such as Jira cards on a Kanban board.\n\nCombine with background.overlay"
      },
      "value": [
        {
          "radius": 1,
          "offset": {
            "x": 0,
            "y": 1
          },
          "color": "N1100",
          "opacity": 0.25
        },
        {
          "radius": 1,
          "offset": {
            "x": 0,
            "y": 0
          },
          "color": "N1100",
          "opacity": 0.31
        }
      ]
    },
    "name": "shadow.card",
    "path": [
      "shadow",
      "card"
    ]
  },
  {
    "attributes": {
      "group": "shadow",
      "state": "active",
      "description": "\nUse for the box shadow of overlay elements, such as modals, dropdown menus, flags, and inline dialogs (i.e. elements that sit on top of the UI).\n\nAlso use for the box shadow of raised cards in a dragged state.\n\nCombine with background.overlay"
    },
    "value": [
      {
        "radius": 12,
        "offset": {
          "x": 0,
          "y": 8
        },
        "color": "#091E42",
        "opacity": 0.15
      },
      {
        "radius": 1,
        "offset": {
          "x": 0,
          "y": 0
        },
        "color": "#091E42",
        "opacity": 0.31
      }
    ],
    "filePath": "src/tokens/atlassian-light/shadow/shadow.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "shadow",
        "state": "active",
        "description": "\nUse for the box shadow of overlay elements, such as modals, dropdown menus, flags, and inline dialogs (i.e. elements that sit on top of the UI).\n\nAlso use for the box shadow of raised cards in a dragged state.\n\nCombine with background.overlay"
      },
      "value": [
        {
          "radius": 12,
          "offset": {
            "x": 0,
            "y": 8
          },
          "color": "N1100",
          "opacity": 0.15
        },
        {
          "radius": 1,
          "offset": {
            "x": 0,
            "y": 0
          },
          "color": "N1100",
          "opacity": 0.31
        }
      ]
    },
    "name": "shadow.overlay",
    "path": [
      "shadow",
      "overlay"
    ]
  },
  {
    "attributes": {
      "group": "raw",
      "state": "active",
      "description": "Transparent token used for backwards compatibility between new and old theming solutions"
    },
    "value": "transparent",
    "filePath": "src/tokens/atlassian-light/utility/utility.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "raw",
        "state": "active",
        "description": "Transparent token used for backwards compatibility between new and old theming solutions"
      },
      "value": "transparent"
    },
    "name": "utility.UNSAFE_util.transparent",
    "path": [
      "utility",
      "UNSAFE_util",
      "transparent"
    ]
  },
  {
    "attributes": {
      "group": "raw",
      "state": "active",
      "description": "Used as a placeholder when a suitable token does not exist"
    },
    "value": "#FA11F2",
    "filePath": "src/tokens/atlassian-light/utility/utility.tsx",
    "isSource": true,
    "original": {
      "attributes": {
        "group": "raw",
        "state": "active",
        "description": "Used as a placeholder when a suitable token does not exist"
      },
      "value": "#FA11F2"
    },
    "name": "utility.UNSAFE_util.MISSING_TOKEN",
    "path": [
      "utility",
      "UNSAFE_util",
      "MISSING_TOKEN"
    ]
  }
];

export default tokens;
