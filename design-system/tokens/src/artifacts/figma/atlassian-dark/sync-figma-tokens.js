
/* eslint-disable no-undef */

// THIS IS AN AUTO-GENERATED FILE DO NOT MODIFY DIRECTLY
// Re-generate by running `yarn build tokens`.
// Read the instructions to use this here:
// `packages/design-system/tokens/src/figma/README.md`
synchronizeFigmaTokens('AtlassianDark', {
  "Color/Accent/BoldBlue": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for blue backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#0C66E4"
  },
  "Color/Accent/BoldGreen": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for green backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#1F845A"
  },
  "Color/Accent/BoldOrange": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for orange backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#B65C02"
  },
  "Color/Accent/BoldPurple": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for purple backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#6E5DC6"
  },
  "Color/Accent/BoldRed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for red backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#CA3521"
  },
  "Color/Accent/BoldTeal": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for teal backgrounds of stronger emphasis when there is no meaning tied to the color, such as bold tags."
    },
    "value": "#1D7F8C"
  },
  "Color/Accent/SubtleBlue": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for blue subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#09326C"
  },
  "Color/Accent/SubtleGreen": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for green subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#164B35"
  },
  "Color/Accent/SubtleMagenta": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for magenta subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#50253F"
  },
  "Color/Accent/SubtleOrange": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for orange subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#5F3811"
  },
  "Color/Accent/SubtlePurple": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for purple subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#352C63"
  },
  "Color/Accent/SubtleRed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for red subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#601E16"
  },
  "Color/Accent/SubtleTeal": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for teal subdued backgrounds when there is no meaning tied to the color, such as colored tags."
    },
    "value": "#1D474C"
  },
  "Color/Background/Sunken": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as a secondary background for the UI"
    },
    "value": "#03040442"
  },
  "Color/Background/Default": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as the primary background for the UI"
    },
    "value": "#161A1D"
  },
  "Color/Background/Card": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for the background of raised cards, such as Jira cards on a Kanban board.\nCombine with shadow.card."
    },
    "value": "#1D2125"
  },
  "Color/Background/Overlay": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse for the background of overlay elements, such as modals, dropdown menus, flags, and inline dialogs (i.e. elements that sit on top of the UI).\n\nAlso use for the background of raised cards in a dragged state.\n\nCombine with shadow.overlay."
    },
    "value": "#22272B"
  },
  "Color/Background/Selected Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for backgrounds of elements in a selected state"
    },
    "value": "#A1BDD914"
  },
  "Color/Background/Selected Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.selected"
    },
    "value": "#A6C5E229"
  },
  "Color/Background/Selected Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.selected"
    },
    "value": "#BFDBF847"
  },
  "Color/Background/Blanket": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for the screen overlay that appears with modal dialogs"
    },
    "value": "#03040442"
  },
  "Color/Background/Disabled": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for backgrounds of elements in a disabled state"
    },
    "value": "#A1BDD914"
  },
  "Color/Background/BoldBrand Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like primary buttons and bold in progress lozenges."
    },
    "value": "#579DFF"
  },
  "Color/Background/BoldBrand Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldBrand"
    },
    "value": "#85B8FF"
  },
  "Color/Background/BoldBrand Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldBrand"
    },
    "value": "#CCE0FF"
  },
  "Color/Background/SubtleBrand Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like information section messages and in progress lozenges."
    },
    "value": "#082145"
  },
  "Color/Background/SubtleBrand Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleBrand"
    },
    "value": "#09326C"
  },
  "Color/Background/SubtleBrand Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleBrand"
    },
    "value": "#0055CC"
  },
  "Color/Background/BoldDanger Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like danger buttons and bold removed lozenges."
    },
    "value": "#F87462"
  },
  "Color/Background/BoldDanger Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldDanger"
    },
    "value": "#FF9C8F"
  },
  "Color/Background/BoldDanger Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldDanger"
    },
    "value": "#FFD2CC"
  },
  "Color/Background/SubtleDanger Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like error section messages and removed lozenges."
    },
    "value": "#391813"
  },
  "Color/Background/SubtleDanger Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleDanger"
    },
    "value": "#601E16"
  },
  "Color/Background/SubtleDanger Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleDanger"
    },
    "value": "#AE2A19"
  },
  "Color/Background/BoldWarning Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like warning buttons and bold moved lozenges."
    },
    "value": "#E2B203"
  },
  "Color/Background/BoldWarning Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldWarning"
    },
    "value": "#F5CD47"
  },
  "Color/Background/BoldWarning Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldWarning"
    },
    "value": "#F8E6A0"
  },
  "Color/Background/SubtleWarning Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like warning section messages and moved lozenges."
    },
    "value": "#3D2E00"
  },
  "Color/Background/SubtleWarning Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleWarning"
    },
    "value": "#533F04"
  },
  "Color/Background/SubtleWarning Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleWarning"
    },
    "value": "#7F5F01"
  },
  "Color/Background/BoldSuccess Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like checked toggles and bold success lozenges."
    },
    "value": "#4BCE97"
  },
  "Color/Background/BoldSuccess Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldSuccess"
    },
    "value": "#7EE2B8"
  },
  "Color/Background/BoldSuccess Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldSuccess"
    },
    "value": "#BAF3DB"
  },
  "Color/Background/SubtleSuccess Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like success section messages and success lozenges. "
    },
    "value": "#133527"
  },
  "Color/Background/SubtleSuccess Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleSuccess"
    },
    "value": "#164B35"
  },
  "Color/Background/SubtleSuccess Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleSuccess"
    },
    "value": "#216E4E"
  },
  "Color/Background/BoldDiscovery Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like onboarding buttons and bold new lozenges."
    },
    "value": "#9F8FEF"
  },
  "Color/Background/BoldDiscovery Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldDiscovery"
    },
    "value": "#B8ACF6"
  },
  "Color/Background/BoldDiscovery Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldDiscovery"
    },
    "value": "#DFD8FD"
  },
  "Color/Background/SubtleDiscovery Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for subdued backgrounds of UI elements like discovery section messages and new lozenges."
    },
    "value": "#231C3F"
  },
  "Color/Background/SubtleDiscovery Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleDiscovery"
    },
    "value": "#352C63"
  },
  "Color/Background/SubtleDiscovery Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleDiscovery"
    },
    "value": "#5E4DB2"
  },
  "Color/Background/BoldNeutral Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "A vibrant background for small UI elements like unchecked toggles and bold default lozenges."
    },
    "value": "#9FADBC"
  },
  "Color/Background/BoldNeutral Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state of background.boldNeutral"
    },
    "value": "#B6C2CF"
  },
  "Color/Background/BoldNeutral Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state of background.boldNeutral"
    },
    "value": "#C7D1DB"
  },
  "Color/Background/TransparentNeutral Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for UIs that don’t have a default background, such as menu items or subtle buttons."
    },
    "value": "#A1BDD914"
  },
  "Color/Background/TransparentNeutral Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for UIs that don’t have a default background, such as menu items or subtle buttons."
    },
    "value": "#A6C5E229"
  },
  "Color/Background/SubtleNeutral Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as the default background of UI elements like buttons, lozenges, and tags."
    },
    "value": "#A1BDD914"
  },
  "Color/Background/SubtleNeutral Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleNeutral"
    },
    "value": "#A6C5E229"
  },
  "Color/Background/SubtleNeutral Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleNeutral"
    },
    "value": "#BFDBF847"
  },
  "Color/Background/SubtleBorderedNeutral Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Hover state for background.subtleBorderedNeutral"
    },
    "value": "#BCD6F00A"
  },
  "Color/Background/SubtleBorderedNeutral Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Pressed state for background.subtleBorderedNeutral"
    },
    "value": "#A1BDD914"
  },
  "Color/Border/Focus": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for focus rings of elements in a focus state"
    },
    "value": "#85B8FF"
  },
  "Color/Border/Neutral": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use to create borders around UI elements such as text fields, checkboxes, and radio buttons, or to visually group or separate UI elements, such as flat cards or side panel dividers"
    },
    "value": "#A6C5E229"
  },
  "Color/IconBorder/Brand": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons or borders representing brand, in-progress, or information, such as the icons in information sections messages.\n\nAlso use for blue icons or borders when there is no meaning tied to the color, such as file type icons."
    },
    "value": "#388BFF"
  },
  "Color/IconBorder/Danger": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons and borders representing critical information, such the icons in error section messages or the borders on invalid text fields.\n\nAlso use for red icons or borders when there is no meaning tied to the color, such as file type icons."
    },
    "value": "#EF5C48"
  },
  "Color/IconBorder/Warning": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons and borders representing semi-urgent information, such as the icons in warning section messages.\n\nAlso use for yellow icons or borders when there is no meaning tied to the color, such as file type icons.\n"
    },
    "value": "#CF9F02"
  },
  "Color/IconBorder/Success": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons and borders representing positive information, such as the icons in success section messages or the borders on validated text fields.\n\nAlso use for green icons or borders when there is no meaning tied to the color, such as file type icons.\n"
    },
    "value": "#2ABB7F"
  },
  "Color/IconBorder/Discovery": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse rarely for icons and borders representing new information, such as the icons in discovery section mesages or the borders in onboarding spotlights.\n\nAlso use for purple icons or borders when there is no meaning tied to the color, such as file type icons.\n"
    },
    "value": "#8F7EE7"
  },
  "Color/Overlay Hover": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as a background overlay for elements in a hover state when their background color cannot change, such as avatars."
    },
    "value": "#BFDBF847"
  },
  "Color/Overlay Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use as a background overlay for elements in a pressed state when their background color cannot change, such as avatars."
    },
    "value": "#A9C5DF7A"
  },
  "Color/Text/Selected": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for text, icons, borders, or other visual indicators in selected states"
    },
    "value": "#579DFF"
  },
  "Color/Text/HighEmphasis": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for primary text, such as body copy, sentence case headers, and buttons"
    },
    "value": "#C7D1DB"
  },
  "Color/Text/MediumEmphasis": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse for secondary text, such navigation, subtle button links, input field labels, and all caps subheadings.\n\nUse for icon-only buttons, or icons paired with text.highEmphasis text\n      "
    },
    "value": "#9FADBC"
  },
  "Color/Text/LowEmphasis": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "\nUse for tertiary text, such as meta-data, breadcrumbs, input field placeholder and helper text.\n\nUse for icons that are paired with text.medEmphasis text"
    },
    "value": "#8696A7"
  },
  "Color/Text/OnBold": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for text and icons when on bold backgrounds"
    },
    "value": "#161A1D"
  },
  "Color/Text/OnBoldWarning": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for text and icons when on bold warning backgrounds"
    },
    "value": "#161A1D"
  },
  "Color/Text/Link Resting": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for links in a resting or hover state. Add an underline for hover states"
    },
    "value": "#579DFF"
  },
  "Color/Text/Link Pressed": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for links in a pressed state"
    },
    "value": "#85B8FF"
  },
  "Color/Text/Brand": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for text on subtle brand backgrounds, such as in progress lozenges, or on subtle blue accent backgrounds, such as colored tags."
    },
    "value": "#85B8FF"
  },
  "Color/Text/Warning": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for text on subtle warning backgrounds, such as in lozenges, or text on subtle warning backgrounds, such as in moved lozenges"
    },
    "value": "#F5CD47"
  },
  "Color/Text/Danger": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for critical text, such as input field error messaging, or text on subtle danger backgrounds, such as in removed lozenges, or text on subtle red accent backgrounds, such as colored tags."
    },
    "value": "#FF9C8F"
  },
  "Color/Text/Success": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for positive text, such as input field success messaging, or text on subtle success backgrounds, such as in success lozenges, or text on subtle green accent backgrounds, such as colored tags."
    },
    "value": "#7EE2B8"
  },
  "Color/Text/Discovery": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use rarely for text on subtle discovery backgrounds, such as in new lozenges, or text on subtle purple accent backgrounds, such as colored tags."
    },
    "value": "#B8ACF6"
  },
  "Color/Text/Disabled": {
    "attributes": {
      "group": "paint",
      "state": "active",
      "description": "Use for text and icons in disabled states"
    },
    "value": "#5C6C7A"
  },
  "Shadow/Card": {
    "attributes": {
      "group": "shadow",
      "state": "active",
      "description": "\nUse for the box shadow of raised card elements, such as Jira cards on a Kanban board.\n\nCombine with background.overlay"
    },
    "value": [
      {
        "radius": 1,
        "offset": {
          "x": 0,
          "y": 1
        },
        "color": "#03040442",
        "opacity": 0.5
      },
      {
        "radius": 1,
        "offset": {
          "x": 0,
          "y": 0
        },
        "color": "#03040442",
        "opacity": 0.5
      }
    ]
  },
  "Shadow/Overlay": {
    "attributes": {
      "group": "shadow",
      "state": "active",
      "description": "\nUse for the box shadow of overlay elements, such as modals, dropdown menus, flags, and inline dialogs (i.e. elements that sit on top of the UI).\n\nAlso use for the box shadow of raised cards in a dragged state.\n\nCombine with background.overlay"
    },
    "value": [
      {
        "radius": 0,
        "spread": 1,
        "color": "#BCD6F00A",
        "offset": {
          "x": 0,
          "y": 0
        },
        "opacity": 0.04,
        "inset": true
      },
      {
        "radius": 12,
        "offset": {
          "x": 0,
          "y": 8
        },
        "color": "#03040442",
        "opacity": 0.36
      },
      {
        "radius": 1,
        "offset": {
          "x": 0,
          "y": 0
        },
        "color": "#03040442",
        "opacity": 0.5
      }
    ]
  }
}, {});
