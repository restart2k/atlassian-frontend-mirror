// THIS IS AN AUTO-GENERATED FILE DO NOT MODIFY DIRECTLY
// Re-generate by running `yarn build tokens`.
const tokens = {
  'color.accent.boldBlue': '--ds-accent-boldBlue',
  'color.accent.boldGreen': '--ds-accent-boldGreen',
  'color.accent.boldOrange': '--ds-accent-boldOrange',
  'color.accent.boldPurple': '--ds-accent-boldPurple',
  'color.accent.boldRed': '--ds-accent-boldRed',
  'color.accent.boldTeal': '--ds-accent-boldTeal',
  'color.accent.subtleBlue': '--ds-accent-subtleBlue',
  'color.accent.subtleGreen': '--ds-accent-subtleGreen',
  'color.accent.subtleMagenta': '--ds-accent-subtleMagenta',
  'color.accent.subtleOrange': '--ds-accent-subtleOrange',
  'color.accent.subtlePurple': '--ds-accent-subtlePurple',
  'color.accent.subtleRed': '--ds-accent-subtleRed',
  'color.accent.subtleTeal': '--ds-accent-subtleTeal',
  'color.background.sunken': '--ds-background-sunken',
  'color.background.default': '--ds-background-default',
  'color.background.card': '--ds-background-card',
  'color.background.overlay': '--ds-background-overlay',
  'color.background.selected.resting': '--ds-background-selected-resting',
  'color.background.selected.hover': '--ds-background-selected-hover',
  'color.background.selected.pressed': '--ds-background-selected-pressed',
  'color.background.blanket': '--ds-background-blanket',
  'color.background.disabled': '--ds-background-disabled',
  'color.background.boldBrand.resting': '--ds-background-boldBrand-resting',
  'color.background.boldBrand.hover': '--ds-background-boldBrand-hover',
  'color.background.boldBrand.pressed': '--ds-background-boldBrand-pressed',
  'color.background.subtleBrand.resting': '--ds-background-subtleBrand-resting',
  'color.background.subtleBrand.hover': '--ds-background-subtleBrand-hover',
  'color.background.subtleBrand.pressed': '--ds-background-subtleBrand-pressed',
  'color.background.boldDanger.resting': '--ds-background-boldDanger-resting',
  'color.background.boldDanger.hover': '--ds-background-boldDanger-hover',
  'color.background.boldDanger.pressed': '--ds-background-boldDanger-pressed',
  'color.background.subtleDanger.resting':
    '--ds-background-subtleDanger-resting',
  'color.background.subtleDanger.hover': '--ds-background-subtleDanger-hover',
  'color.background.subtleDanger.pressed':
    '--ds-background-subtleDanger-pressed',
  'color.background.boldWarning.resting': '--ds-background-boldWarning-resting',
  'color.background.boldWarning.hover': '--ds-background-boldWarning-hover',
  'color.background.boldWarning.pressed': '--ds-background-boldWarning-pressed',
  'color.background.subtleWarning.resting':
    '--ds-background-subtleWarning-resting',
  'color.background.subtleWarning.hover': '--ds-background-subtleWarning-hover',
  'color.background.subtleWarning.pressed':
    '--ds-background-subtleWarning-pressed',
  'color.background.boldSuccess.resting': '--ds-background-boldSuccess-resting',
  'color.background.boldSuccess.hover': '--ds-background-boldSuccess-hover',
  'color.background.boldSuccess.pressed': '--ds-background-boldSuccess-pressed',
  'color.background.subtleSuccess.resting':
    '--ds-background-subtleSuccess-resting',
  'color.background.subtleSuccess.hover': '--ds-background-subtleSuccess-hover',
  'color.background.subtleSuccess.pressed':
    '--ds-background-subtleSuccess-pressed',
  'color.background.boldDiscovery.resting':
    '--ds-background-boldDiscovery-resting',
  'color.background.boldDiscovery.hover': '--ds-background-boldDiscovery-hover',
  'color.background.boldDiscovery.pressed':
    '--ds-background-boldDiscovery-pressed',
  'color.background.subtleDiscovery.resting':
    '--ds-background-subtleDiscovery-resting',
  'color.background.subtleDiscovery.hover':
    '--ds-background-subtleDiscovery-hover',
  'color.background.subtleDiscovery.pressed':
    '--ds-background-subtleDiscovery-pressed',
  'color.background.boldNeutral.resting': '--ds-background-boldNeutral-resting',
  'color.background.boldNeutral.hover': '--ds-background-boldNeutral-hover',
  'color.background.boldNeutral.pressed': '--ds-background-boldNeutral-pressed',
  'color.background.transparentNeutral.hover':
    '--ds-background-transparentNeutral-hover',
  'color.background.transparentNeutral.pressed':
    '--ds-background-transparentNeutral-pressed',
  'color.background.subtleNeutral.resting':
    '--ds-background-subtleNeutral-resting',
  'color.background.subtleNeutral.hover': '--ds-background-subtleNeutral-hover',
  'color.background.subtleNeutral.pressed':
    '--ds-background-subtleNeutral-pressed',
  'color.background.subtleBorderedNeutral.resting':
    '--ds-background-subtleBorderedNeutral-resting',
  'color.background.subtleBorderedNeutral.pressed':
    '--ds-background-subtleBorderedNeutral-pressed',
  'color.border.focus': '--ds-border-focus',
  'color.border.neutral': '--ds-border-neutral',
  'color.iconBorder.brand': '--ds-iconBorder-brand',
  'color.iconBorder.danger': '--ds-iconBorder-danger',
  'color.iconBorder.warning': '--ds-iconBorder-warning',
  'color.iconBorder.success': '--ds-iconBorder-success',
  'color.iconBorder.discovery': '--ds-iconBorder-discovery',
  'color.overlay.hover': '--ds-overlay-hover',
  'color.overlay.pressed': '--ds-overlay-pressed',
  'color.text.selected': '--ds-text-selected',
  'color.text.highEmphasis': '--ds-text-highEmphasis',
  'color.text.mediumEmphasis': '--ds-text-mediumEmphasis',
  'color.text.lowEmphasis': '--ds-text-lowEmphasis',
  'color.text.onBold': '--ds-text-onBold',
  'color.text.onBoldWarning': '--ds-text-onBoldWarning',
  'color.text.link.resting': '--ds-text-link-resting',
  'color.text.link.pressed': '--ds-text-link-pressed',
  'color.text.brand': '--ds-text-brand',
  'color.text.warning': '--ds-text-warning',
  'color.text.danger': '--ds-text-danger',
  'color.text.success': '--ds-text-success',
  'color.text.discovery': '--ds-text-discovery',
  'color.text.disabled': '--ds-text-disabled',
  'shadow.card': '--ds-card',
  'shadow.overlay': '--ds-overlay',
  'utility.UNSAFE_util.transparent': '--ds-UNSAFE_util-transparent',
  'utility.UNSAFE_util.MISSING_TOKEN': '--ds-UNSAFE_util-MISSING_TOKEN',
} as const;

export type CSSTokenMap = {
  'color.accent.boldBlue': 'var(--ds-accent-boldBlue)';
  'color.accent.boldGreen': 'var(--ds-accent-boldGreen)';
  'color.accent.boldOrange': 'var(--ds-accent-boldOrange)';
  'color.accent.boldPurple': 'var(--ds-accent-boldPurple)';
  'color.accent.boldRed': 'var(--ds-accent-boldRed)';
  'color.accent.boldTeal': 'var(--ds-accent-boldTeal)';
  'color.accent.subtleBlue': 'var(--ds-accent-subtleBlue)';
  'color.accent.subtleGreen': 'var(--ds-accent-subtleGreen)';
  'color.accent.subtleMagenta': 'var(--ds-accent-subtleMagenta)';
  'color.accent.subtleOrange': 'var(--ds-accent-subtleOrange)';
  'color.accent.subtlePurple': 'var(--ds-accent-subtlePurple)';
  'color.accent.subtleRed': 'var(--ds-accent-subtleRed)';
  'color.accent.subtleTeal': 'var(--ds-accent-subtleTeal)';
  'color.background.sunken': 'var(--ds-background-sunken)';
  'color.background.default': 'var(--ds-background-default)';
  'color.background.card': 'var(--ds-background-card)';
  'color.background.overlay': 'var(--ds-background-overlay)';
  'color.background.selected.resting': 'var(--ds-background-selected-resting)';
  'color.background.selected.hover': 'var(--ds-background-selected-hover)';
  'color.background.selected.pressed': 'var(--ds-background-selected-pressed)';
  'color.background.blanket': 'var(--ds-background-blanket)';
  'color.background.disabled': 'var(--ds-background-disabled)';
  'color.background.boldBrand.resting': 'var(--ds-background-boldBrand-resting)';
  'color.background.boldBrand.hover': 'var(--ds-background-boldBrand-hover)';
  'color.background.boldBrand.pressed': 'var(--ds-background-boldBrand-pressed)';
  'color.background.subtleBrand.resting': 'var(--ds-background-subtleBrand-resting)';
  'color.background.subtleBrand.hover': 'var(--ds-background-subtleBrand-hover)';
  'color.background.subtleBrand.pressed': 'var(--ds-background-subtleBrand-pressed)';
  'color.background.boldDanger.resting': 'var(--ds-background-boldDanger-resting)';
  'color.background.boldDanger.hover': 'var(--ds-background-boldDanger-hover)';
  'color.background.boldDanger.pressed': 'var(--ds-background-boldDanger-pressed)';
  'color.background.subtleDanger.resting': 'var(--ds-background-subtleDanger-resting)';
  'color.background.subtleDanger.hover': 'var(--ds-background-subtleDanger-hover)';
  'color.background.subtleDanger.pressed': 'var(--ds-background-subtleDanger-pressed)';
  'color.background.boldWarning.resting': 'var(--ds-background-boldWarning-resting)';
  'color.background.boldWarning.hover': 'var(--ds-background-boldWarning-hover)';
  'color.background.boldWarning.pressed': 'var(--ds-background-boldWarning-pressed)';
  'color.background.subtleWarning.resting': 'var(--ds-background-subtleWarning-resting)';
  'color.background.subtleWarning.hover': 'var(--ds-background-subtleWarning-hover)';
  'color.background.subtleWarning.pressed': 'var(--ds-background-subtleWarning-pressed)';
  'color.background.boldSuccess.resting': 'var(--ds-background-boldSuccess-resting)';
  'color.background.boldSuccess.hover': 'var(--ds-background-boldSuccess-hover)';
  'color.background.boldSuccess.pressed': 'var(--ds-background-boldSuccess-pressed)';
  'color.background.subtleSuccess.resting': 'var(--ds-background-subtleSuccess-resting)';
  'color.background.subtleSuccess.hover': 'var(--ds-background-subtleSuccess-hover)';
  'color.background.subtleSuccess.pressed': 'var(--ds-background-subtleSuccess-pressed)';
  'color.background.boldDiscovery.resting': 'var(--ds-background-boldDiscovery-resting)';
  'color.background.boldDiscovery.hover': 'var(--ds-background-boldDiscovery-hover)';
  'color.background.boldDiscovery.pressed': 'var(--ds-background-boldDiscovery-pressed)';
  'color.background.subtleDiscovery.resting': 'var(--ds-background-subtleDiscovery-resting)';
  'color.background.subtleDiscovery.hover': 'var(--ds-background-subtleDiscovery-hover)';
  'color.background.subtleDiscovery.pressed': 'var(--ds-background-subtleDiscovery-pressed)';
  'color.background.boldNeutral.resting': 'var(--ds-background-boldNeutral-resting)';
  'color.background.boldNeutral.hover': 'var(--ds-background-boldNeutral-hover)';
  'color.background.boldNeutral.pressed': 'var(--ds-background-boldNeutral-pressed)';
  'color.background.transparentNeutral.hover': 'var(--ds-background-transparentNeutral-hover)';
  'color.background.transparentNeutral.pressed': 'var(--ds-background-transparentNeutral-pressed)';
  'color.background.subtleNeutral.resting': 'var(--ds-background-subtleNeutral-resting)';
  'color.background.subtleNeutral.hover': 'var(--ds-background-subtleNeutral-hover)';
  'color.background.subtleNeutral.pressed': 'var(--ds-background-subtleNeutral-pressed)';
  'color.background.subtleBorderedNeutral.resting': 'var(--ds-background-subtleBorderedNeutral-resting)';
  'color.background.subtleBorderedNeutral.pressed': 'var(--ds-background-subtleBorderedNeutral-pressed)';
  'color.border.focus': 'var(--ds-border-focus)';
  'color.border.neutral': 'var(--ds-border-neutral)';
  'color.iconBorder.brand': 'var(--ds-iconBorder-brand)';
  'color.iconBorder.danger': 'var(--ds-iconBorder-danger)';
  'color.iconBorder.warning': 'var(--ds-iconBorder-warning)';
  'color.iconBorder.success': 'var(--ds-iconBorder-success)';
  'color.iconBorder.discovery': 'var(--ds-iconBorder-discovery)';
  'color.overlay.hover': 'var(--ds-overlay-hover)';
  'color.overlay.pressed': 'var(--ds-overlay-pressed)';
  'color.text.selected': 'var(--ds-text-selected)';
  'color.text.highEmphasis': 'var(--ds-text-highEmphasis)';
  'color.text.mediumEmphasis': 'var(--ds-text-mediumEmphasis)';
  'color.text.lowEmphasis': 'var(--ds-text-lowEmphasis)';
  'color.text.onBold': 'var(--ds-text-onBold)';
  'color.text.onBoldWarning': 'var(--ds-text-onBoldWarning)';
  'color.text.link.resting': 'var(--ds-text-link-resting)';
  'color.text.link.pressed': 'var(--ds-text-link-pressed)';
  'color.text.brand': 'var(--ds-text-brand)';
  'color.text.warning': 'var(--ds-text-warning)';
  'color.text.danger': 'var(--ds-text-danger)';
  'color.text.success': 'var(--ds-text-success)';
  'color.text.discovery': 'var(--ds-text-discovery)';
  'color.text.disabled': 'var(--ds-text-disabled)';
  'shadow.card': 'var(--ds-card)';
  'shadow.overlay': 'var(--ds-overlay)';
  'utility.UNSAFE_util.transparent': 'var(--ds-UNSAFE_util-transparent)';
  'utility.UNSAFE_util.MISSING_TOKEN': 'var(--ds-UNSAFE_util-MISSING_TOKEN)';
};

export type CSSToken = CSSTokenMap[keyof CSSTokenMap];

export default tokens;
