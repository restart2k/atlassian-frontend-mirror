export const ADDON_ID = 'ads-tokens';
export const PANEL_ID = `${ADDON_ID}/panel`;
export const PANEL_TITLE = `Design Tokens`;
export const EVENT_THEME_CHANGED = `${ADDON_ID}/on-theme-change`;
export const DECORATOR_ID = 'withDesignTokens';
export const DECORATOR_PARAM = 'DesignTokens';
